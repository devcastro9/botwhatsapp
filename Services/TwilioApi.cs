﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace BotWhatsapp.Services;

public class TwilioApi
{
    public readonly HttpClient _httpClient;
    public TwilioApi(HttpClient httpClient)
    {
        _httpClient = httpClient;
    }

    public async Task<string> SendMessage(string Message, string Sid, string Token)
    {
        TwilioClient.Init(Sid, Token);
        var messageOptions = new CreateMessageOptions( 
            new PhoneNumber("whatsapp:+59178763833")); 
        messageOptions.From = new PhoneNumber("whatsapp:+14155238886");    
        messageOptions.Body = $"{Message}\nMensaje enviado a {DateTime.Now:yyyy-MM-dd}";
        var message = await MessageResource.CreateAsync(messageOptions);
        return message.Body;
    }
}
